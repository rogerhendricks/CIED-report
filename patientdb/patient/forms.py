from django import forms
from .models import Client, Service, Cardiologist, General_Practitioner
from bootstrap_datepicker_plus import DateTimePickerInput
#from bootstrap_modal_forms.forms import BSModalForm
#from bootstrap_modal_forms.mixins import PopRequestMixin, CreateUpdateAjaxMixin


    
class ClientForm(forms.ModelForm):
    
    #cardiologist = forms.ModelChoiceField(queryset=Cardiologist.objects.all().order_by('last_name'), to_field_name="last_name")

    class Meta:
        model = Client
        fields= [
                    'record_number',
                    'first_name', 
                    'last_name',
                    'dob', 
                    'device', 
                    'implant_date', 
                    'device_serial', 
                    'cardiologist',
                    'general_practitioner',
                    ]
        widgets = {
            'dob': forms.DateInput(attrs={'class': 'datepicker', 'type':'date'}),
            'implant_date': forms.DateInput(attrs={'class': 'datepicker', 'type':'date'})
            }
    

class ServiceForm(forms.ModelForm):

   class Meta:
        model = Service
        fields = [
                    'client',
                    'MDC_IDC_SESS_TYPE',
                    'MDC_IDC_SESS_DTM',
                    'MDC_IDC_MSMT_BATTERY_VOLTAGE',
                    'MDC_IDC_MSMT_CAP_CHARGE_TIME',
                    'MDC_IDC_STAT_BRADY_RA_PERCENT_PACED',
                    'MDC_IDC_STAT_BRADY_RV_PERCENT_PACED',
                    'MDC_IDC_STAT_CRT_LV_PERCENT_PACED',
                    'MDC_IDC_MSMT_LEADCHNL_RA_SENSING_INTR_AMPL_MEAN',
                    'MDC_IDC_MSMT_LEADCHNL_RA_IMPEDANCE_VALUE',
                    'MDC_IDC_MSMT_LEADCHNL_RA_PACING_THRESHOLD_AMPLITUDE',
                    'MDC_IDC_MSMT_LEADCHNL_RA_PACING_THRESHOLD_PULSEWIDTH',
                    'MDC_IDC_MSMT_LEADCHNL_RV_SENSING_INTR_AMPL_MEAN',
                    'MDC_IDC_MSMT_LEADCHNL_RV_IMPEDANCE_VALUE',
                    'MDC_IDC_MSMT_LEADCHNL_RV_PACING_THRESHOLD_AMPLITUDE',
                    'MDC_IDC_MSMT_LEADCHNL_RV_PACING_THRESHOLD_PULSEWIDTH',
                    'MDC_IDC_MSMT_LEADHVCHNL_IMPEDANCE',
                    'MDC_IDC_SESS_COMMENTS',
                    ]
        labels = {
            'MDC_IDC_SESS_TYPE': 'Service Type',
            'MDC_IDC_SESS_DTM': 'Service Date',
            'MDC_IDC_MSMT_BATTERY_VOLTAGE': 'Battery Voltage',
            'MDC_IDC_MSMT_CAP_CHARGE_TIME': 'Battery Charge Time',
            'MDC_IDC_STAT_BRADY_RA_PERCENT_PACED': 'RA Pacing Percentage',
            'MDC_IDC_STAT_BRADY_RV_PERCENT_PACED': 'RV Pacing Percentage',
            'MDC_IDC_STAT_CRT_LV_PERCENT_PACED': 'LV Pacing Percentage',
            'MDC_IDC_MSMT_LEADCHNL_RA_SENSING_INTR_AMPL_MEAN':'RA Sensing Amplitude',
            'MDC_IDC_MSMT_LEADCHNL_RA_IMPEDANCE_VALUE':'RA Pacing Impedance',
            'MDC_IDC_MSMT_LEADCHNL_RA_PACING_THRESHOLD_AMPLITUDE':'RA Pacing Theshold',
            'MDC_IDC_MSMT_LEADCHNL_RA_PACING_THRESHOLD_PULSEWIDTH':'RA Pacing Pulse Width',
            'MDC_IDC_MSMT_LEADCHNL_RV_SENSING_INTR_AMPL_MEAN':'RV Sensing Amplitude',
            'MDC_IDC_MSMT_LEADCHNL_RV_IMPEDANCE_VALUE':'RV Pacing Impedance',
            'MDC_IDC_MSMT_LEADCHNL_RV_PACING_THRESHOLD_AMPLITUDE':'RV Pacing Theshold',
            'MDC_IDC_MSMT_LEADCHNL_RV_PACING_THRESHOLD_PULSEWIDTH':'RV Pacing Pulse Width',
            'MDC_IDC_MSMT_LEADHVCHNL_IMPEDANCE':'RV Shock Pacing Impedance',
            'MDC_IDC_SESS_COMMENTS': 'Comments',
            }
        widgets = {
            'client': forms.HiddenInput(),
            'MDC_IDC_SESS_DTM': DateTimePickerInput(attrs={'class': 'dateicker', 'type':'datetime'})  
            }
        

class SearchForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('last_name',)


class CardiologistForm(forms.ModelForm):
    class Meta: 
        model = Cardiologist
        fields = ('first_name','last_name','str_address','ct_address','pc_address','st_address','phone_1','phone_2', 'email')
        labels = {
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'str_address': 'Street',
            'ct_address': 'City',
            'pc_address':'Post Code',
            'st_address': 'State',
            'phone_1': 'Phone 1',
            'phone_2': 'Phone 2',
            'email': 'Email'
            }


class General_PractitionerForm(forms.ModelForm):
    class Meta: 
        model = General_Practitioner
        fields = ('first_name','last_name','str_address','ct_address','pc_address','st_address','phone_1','phone_2', 'email')
        labels = {
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'str_address': 'Street',
            'ct_address': 'City',
            'pc_address':'Post Code',
            'st_address': 'State',
            'phone_1': 'Phone 1',
            'phone_2': 'Phone 2',
            'email': 'Email'
            }


