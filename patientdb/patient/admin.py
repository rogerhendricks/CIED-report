from django.contrib import admin

# Register your models here.
from .models import Client, Service, Cardiologist, General_Practitioner, Device


admin.site.register(Service)

admin.site.site_header = "Master Control"
admin.site.site_title = "Remote Admin"
admin.site.index_title = "Welcome to Master Control"

def full_name(obj):
    return ("%s %s" % (obj.first_name, obj.last_name)).upper()


class ServiceInLine(admin.StackedInline):
    model = Service
    ordering = ('-MDC_IDC_SESS_DTM',)
    extra = 0
    classes = ['collapse','extrapretty','wide',]

@admin.register(Cardiologist)
class CardiologistAdmin(admin.ModelAdmin):
    pass

@admin.register(General_Practitioner)
class General_PractitionerAdmin(admin.ModelAdmin):
    pass

@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    pass

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    inlines = [
        ServiceInLine,
    ]
    list_display = ['record_number', full_name, 'device', 'device_serial','implant_date']
    search_fields = ['last_name', 'record_number']


