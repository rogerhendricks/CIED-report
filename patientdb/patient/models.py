from django.db import models as db
from django.urls import reverse
from django.core.validators import FileExtensionValidator, MinValueValidator, MaxValueValidator
from datetime import datetime
def DefaultGeneralPractitioner():
    return 1

def DefaultCardiologist():
    return 1


class General_Practitioner(db.Model):
    
    state_choices = (
        ('New South Wales','New South Wales'),
        ('Queensland','Queensland'),
        ('Victoria','Victoria'),
        ('Canberra','Canberra'),
        ('Northern Territory','Northern Territory'),
        ('Western Australia','Western Australia'),
        ('South Australia','South Australia'),
    )

    id = db.AutoField(primary_key=True)
    first_name = db.CharField(max_length=30)
    last_name = db.CharField(max_length=30)
    str_address = db.CharField(max_length=120, null=True)
    ct_address = db.CharField(max_length=120, null=True)
    pc_address = db.PositiveIntegerField(null=True)
    st_address = db.CharField(max_length=30, choices=state_choices, null=True)
    phone_1 = db.PositiveIntegerField(null=True)
    phone_2 = db.PositiveIntegerField(null=True)
    email = db.EmailField(max_length=120, null=True)

    def get_absolute_url(self):
            return reverse('client:generalpractitioner_detail', kwargs={"pk": self.pk})

    def __str__(self):
        return '%s %s' % ( self.first_name, self.last_name )


class Cardiologist(db.Model):
    
    #doc_type_choices = (
        #('Surgeon','Surgeon'),
        #('Cardiologist','Cardiologist'),
        #('General Practitioner','General Practitioner'),
    #)
    state_choices = (
        ('New South Wales','New South Wales'),
        ('Queensland','Queensland'),
        ('Victoria','Victoria'),
        ('Canberra','Canberra'),
        ('Northern Territory','Northern Territory'),
        ('Western Australia','Western Australia'),
        ('South Australia','South Australia'),
    )

    id = db.AutoField(primary_key=True)
    first_name = db.CharField(max_length=30)
    last_name = db.CharField(max_length=30)
    str_address = db.CharField(max_length=120, null=True)
    ct_address = db.CharField(max_length=120, null=True)
    pc_address = db.PositiveIntegerField(null=True)
    st_address = db.CharField(max_length=30, choices=state_choices, null=True)
    phone_1 = db.PositiveIntegerField(null=True)
    phone_2 = db.PositiveIntegerField(null=True)
    email = db.EmailField(max_length=120, null=True)
    #doc_type = db.CharField(max_length=30, choices=doc_type_choices, null=True)

    def get_absolute_url(self):
            return reverse('client:cardiologist_detail', kwargs={"pk": self.pk})

    def __str__(self):
        return '%s %s' % ( self.first_name, self.last_name )


class Device(db.Model):
    # columns!
    
    dev_man_choices = (
    ('Abbot','Abbot'),
    ('Biotronik','Biotronik'),
    ('Boston Scientific','Boston Scientific'),
    ('Medtronic','Medtronic'),
    ('Sorin','Sorin'),
    )
    id = db.AutoField(primary_key=True)
    device_name = db.CharField(max_length=30, null=True)
    device_manufacturer = db.CharField(max_length=30, choices=dev_man_choices, null=True)
    bol_voltage = db.DecimalField(max_digits=3, decimal_places=2, null=True)
    eri_voltage = db.DecimalField(max_digits=3, decimal_places=2, null=True)
    model_number = db.CharField(max_length=15, null=True)
    mri_compatability = db.BooleanField(default=False)

    def __str__(self):
        return '%s %s' % ( self.device_manufacturer, self.device_name )


class Client(db.Model):

    dev_man_choices = (
    ('Abbot','Abbot'),
    ('Biotronik','Biotronik'),
    ('Boston Scientific','Boston Scientific'),
    ('Medtronic','Medtronic'),
    ('Sorin','Sorin'),
    )
    # columns!
    id = db.AutoField(primary_key=True)
    first_name = db.CharField(max_length=30)
    last_name = db.CharField(max_length=30)
    dob = db.DateField()
    record_number = db.PositiveIntegerField(null=True)
    implant_date=db.DateField(null=True)
    device_serial=db.CharField(max_length=30, null=True)
    cardiologist = db.ForeignKey(Cardiologist, default=DefaultCardiologist ,on_delete=db.DO_NOTHING)
    general_practitioner = db.ForeignKey(General_Practitioner, default=DefaultGeneralPractitioner, on_delete=db.DO_NOTHING)
    device = db.ForeignKey(Device, default=None, on_delete=db.DO_NOTHING)


    def get_absolute_url(self):
        return reverse('client:client_detail', kwargs={"pk": self.pk})


    def __str__(self):
        return '%s %s %s' % ( self.id, self.record_number, self.last_name )


class Service(db.Model):

    service_type_choices = (
        ('In Clinic Periodic', 'In Clinic Periodic'),
        ('In Clinic Call Back','In Clinic Call Back'),
        ('Remote Periodic','Remote Periodic'),
        ('Remote Early Detection','Remote Early Detection'),
    )

    # columns!
    id = db.AutoField(primary_key=True)
    MDC_IDC_SESS_COMMENTS = db.TextField(max_length=500)
    MDC_IDC_SESS_TYPE = db.CharField(max_length=25, choices=service_type_choices, default='In Clinic Periodic')
    MDC_IDC_MSMT_BATTERY_VOLTAGE = db.DecimalField(max_digits=4, decimal_places=2)
    MDC_IDC_MSMT_CAP_CHARGE_TIME = db.DecimalField(max_digits=4, decimal_places=2, null=True)
    MDC_IDC_SESS_DTM = db.DateTimeField(default=datetime.now)
    MDC_IDC_STAT_BRADY_RA_PERCENT_PACED = db.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)], null=True)
    MDC_IDC_STAT_BRADY_RV_PERCENT_PACED = db.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)], null=True)
    MDC_IDC_STAT_CRT_LV_PERCENT_PACED = db.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)], null=True)
    MDC_IDC_MSMT_LEADCHNL_RA_SENSING_INTR_AMPL_MEAN = db.DecimalField(max_digits=4, decimal_places=2, null=True)
    MDC_IDC_MSMT_LEADCHNL_RA_IMPEDANCE_VALUE = db.IntegerField(validators=[MinValueValidator(150), MaxValueValidator(3000)], null=True)
    MDC_IDC_MSMT_LEADCHNL_RA_PACING_THRESHOLD_AMPLITUDE = db.DecimalField(max_digits=4, decimal_places=2, null=True)
    MDC_IDC_MSMT_LEADCHNL_RA_PACING_THRESHOLD_PULSEWIDTH = db.DecimalField(max_digits=4, decimal_places=2, null=True)
    MDC_IDC_MSMT_LEADCHNL_RV_SENSING_INTR_AMPL_MEAN = db.DecimalField(max_digits=4, decimal_places=2, null=True)
    MDC_IDC_MSMT_LEADCHNL_RV_IMPEDANCE_VALUE = db.IntegerField(validators=[MinValueValidator(150), MaxValueValidator(3000)], null=True)
    MDC_IDC_MSMT_LEADCHNL_RV_PACING_THRESHOLD_AMPLITUDE = db.DecimalField(max_digits=4, decimal_places=2, null=True)
    MDC_IDC_MSMT_LEADCHNL_RV_PACING_THRESHOLD_PULSEWIDTH = db.DecimalField(max_digits=4, decimal_places=2, null=True)
    MDC_IDC_MSMT_LEADHVCHNL_IMPEDANCE = db.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(200)], null=True)
    #shock_imp_svc = db.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(200)], null=True)
    client = db.ForeignKey('Client', on_delete=db.CASCADE)

    class Meta:
        ordering = ('-MDC_IDC_SESS_DTM',)

    def get_absolute_url(self):
        return reverse('client:service_detail', kwargs={"pk": self.pk})

    def __str__(self):
        return self.MDC_IDC_SESS_TYPE



