from django.urls import path, re_path
from django.conf.urls import url
from django.conf.urls.static import static
from . import views
from django.conf import settings
from patient.views import ( index, ClientView, 
                        ClientDetailView, 
                        ClientCreate, 
                        ClientDelete, 
                        ClientUpdate, 
                        SearchList, 
                        ServiceView, 
                        ServiceDetailView, 
                        ServiceCreate, 
                        ServiceUpdate, 
                        ServiceDelete,
                        PrintPdf, 
                        CardiologistCreate, 
                        CardiologistView, 
                        CardiologistDetailView, 
                        CardiologistDelete, 
                        CardiologistUpdate, 
                        CardiologistSearchList,
                        GeneralPractitionerCreate, 
                        GeneralPractitionerView, 
                        GeneralPractitionerDetailView, 
                        GeneralPractitionerDelete, 
                        GeneralPractitionerUpdate, 
                        GeneralPractitionerSearchList
                         )
                   
# App name to use                      
app_name = 'client'

# URL patterns 

urlpatterns = [
    path('client/', ClientView.as_view(), name='client_index'),
    path('', index, name='index'),
    #path('<int:pk>/detail/', ClientDetailView.as_view(), name='detail'),

    # /clients/add-delete-update-search
    path('client/new/', ClientCreate.as_view(), name='client_new'),
    path('client/<int:pk>/delete', ClientDelete.as_view(), name='client_delete'),
    path('client/<int:pk>/update', ClientUpdate.as_view(), name='client_update'),
    path('client/<int:pk>/details', ClientDetailView.as_view(), name='client_detail'),
    path('client/search', SearchList.as_view(), name='search'),
    #/services
    path('client/<int:pk>/history', ServiceView.as_view(), name='history'),
    path('client/<int:client.pk>/service/<int:pk>/detail', ServiceDetailView.as_view(), name='service_detail'),
    path('client/<int:client.pk>/service/<int:pk>/detail/pdf_html/', PrintPdf.as_view(), name='service_html_pdf'),
    #path('client/service/add', ServiceCreate.as_view(), name='service_add'),
    path('client/<int:pk>/service/add', ServiceCreate.as_view(), name='service_new'),
    path('client/<int:client.pk>/service/<int:pk>/update', ServiceUpdate.as_view(), name='service_update'),
    path('client/<int:pk>/service/<int:service.pk>/delete', ServiceDelete.as_view(), name='service_delete'),


    # /doctors
    #path('cardiologist/new/',CardiologistCreate.as_view() , name='cardiologist_new'),
    #path('cardiologist/index', CardiologistView.as_view(), name='cardiologist_index'),
    #path('cardiologist/<int:pk>/cardiologist_detail/', CardiologistDetailView.as_view(), name='cardiologist_detail'),
    #path('cardiologist/<int:pk>/delete', CardiologistDelete.as_view(), name='cardiologist_delete'),
    #path('cardiologist/<int:pk>/update', CardiologistUpdate.as_view(), name='cardiologist_update'),
    #path('cardiologist/search', CardiologistSearchList.as_view(), name='cardiologist_search'),


    #path('gp/new/',GeneralPractitionerCreate.as_view() , name='generalpractitioner_new'),
    #path('gp/index', GeneralPractitionerView.as_view(), name='generalpractitioner_index'),
    #path('gp/<int:pk>/details/', GeneralPractitionerDetailView.as_view(), name='generalpractitioner_detail'),
    #path('gp/<int:pk>/delete', GeneralPractitionerDelete.as_view(), name='generalpractitioner_delete'),
    #path('gp/<int:pk>/update', GeneralPractitionerUpdate.as_view(), name='generalpactitioner_update'),
    #path('gp/search', GeneralPractitionerSearchList.as_view(), name='generalpractitioner_search'),

]