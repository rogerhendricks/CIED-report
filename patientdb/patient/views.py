from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView, FormView, View
from django.views.generic.detail import SingleObjectMixin
from .models import Client, Service, Cardiologist, General_Practitioner, Device
from django.http import HttpResponse
#from .utils import render_to_pdf
from django.shortcuts import redirect
from .forms import SearchForm, ClientForm, CardiologistForm, General_PractitionerForm, ServiceForm
from django.contrib import messages
from django.urls import	reverse_lazy, reverse
from django.db.models import Q
from django.utils import timezone
import datetime
from django.http import HttpResponse, HttpResponseRedirect
from django_weasyprint import WeasyTemplateResponseMixin, WeasyTemplateView

from django.conf import settings
from django.core.mail import BadHeaderError, send_mail
from django.template import Context
from django.template.loader import render_to_string
from weasyprint import HTML, CSS

from itertools import chain

# Main page
def index(request):
    return render(request, 'clients/index.html')


# Clients
class ClientView(ListView):
    template_name = 'clients/client_index.html'
    context_object_name = 'all_clients'
    paginate_by = 10

    def get_queryset(self):
        return Client.objects.all()


class ClientDetailView(DetailView, DeleteView, CreateView):
    model = Client
    template_name = 'clients/client_detail.html'
    success_url = reverse_lazy('client:index')
    form_class = ClientForm


class ClientCreate(CreateView):
   model = Client
   template_name = 'clients/client_new.html'
   form_class = ClientForm

class ClientUpdate(UpdateView):
    model = Client
    form_class = ClientForm
    template_name_suffix = '_update_form'

class ClientDelete(DeleteView):
    model = Client
    success_url = reverse_lazy('client:index')


class SearchList(ListView):
    template_name = 'clients/client_index.html'
    model = Client
    context_object_name = 'all_clients'

    def get_queryset(self):
        search = self.request.GET.get('search')
        queryset = Client.objects.filter(Q(last_name__icontains=search)|Q(first_name__icontains=search)|Q(record_number__icontains=search))
        return queryset


# Services
# search services from ServiceListView
class ServiceSearchList():
    template_name = ''
    model = Service
    context_object_name = ''

    def get_queryset(self):
        pass


class ServiceCreate(CreateView):
    template_name = 'clients/oos/service_new.html'
    form_class = ServiceForm

    def get_initial(self, **kwargs):
        initial = super(ServiceCreate, self).get_initial()
        initial['client'] = self.kwargs.get('pk')
        return initial



class ServiceView(SingleObjectMixin, ListView):
    template_name = 'clients/oos/service_history.html'
    context_object_name = 'all_services'
    paginate_by = 10

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Client.objects.all())
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get the context
        context = super().get_context_data(**kwargs)
        # Create any data and add it to the context
        context['client'] = self.object
        return context

    def get_queryset(self):
        queryset = self.object.service_set.all()
        #queryset2 = self.object.procedure_set.all()
        #queryset = list (chain(queryset1, queryset2))
        return queryset


class ServiceDetailView(DetailView, DeleteView, CreateView):
    model = Service
    template_name = 'clients/oos/service_detail.html'
    success_url = reverse_lazy('client:service_detail')
    form_class = ServiceForm
    def get_queryset(self, *args, **kwargs):
        queryset = Service.objects.filter(id=self.kwargs.get('pk')).prefetch_related('client')
        return queryset


#class ServiceCreate(CreateView):
#    template_name = 'clients/service/service_new.html'
#    model = Service
#    form_class = ServiceForm


class ServiceUpdate(UpdateView):
    model = Service
    form_class = ServiceForm
    #template_name_suffix = '_update_form'
    template_name = 'clients/oos/service_update.html'
    success_url = reverse_lazy('client:service_detail')

    def get_initial(self, **kwargs):
        initial = super(ServiceUpdate, self).get_initial()
        initial['client'] = self.kwargs.get('client_id')
        return initial


class ServiceDelete(DeleteView):
    model = Service
    success_url = reverse_lazy('client:index')


# adding service render to pdf
class PrintPdf(View):
    def get(self, request, *args, **kwargs):
        queryset = Service.objects.filter(id=self.kwargs.get('pk')).select_related('client').prefetch_related(
            'client__cardiologist').values().values('id', 'MDC_IDC_MSMT_BATTERY_VOLTAGE', 'MDC_IDC_SESS_TYPE', 'MDC_IDC_SESS_DTM',
                                               'client_id', 'client_id__last_name', 'client_id__first_name',
                                               'client_id__cardiologist__id', 'client_id__cardiologist__first_name',
                                               'client_id__cardiologist__last_name', 'client_id__cardiologist__str_address',
                                               'client_id__cardiologist__st_address', 'client_id__cardiologist__ct_address','client_id__cardiologist__pc_address')[0]
        html_template = render_to_string('clients/pdf/pdf_detail.html', queryset)

        pdf_file = HTML(string=html_template, base_url=request.build_absolute_uri()).write_pdf()
        response = HttpResponse(pdf_file, content_type='application/pdf')
        response['Content-Disposition'] = 'filename="home_page.pdf"'
        return response



# Cardiologist

class CardiologistCreate(CreateView):
    template_name = 'clients/cardiologist/doctor_new.html'
    form_class = CardiologistForm
    success_url = reverse_lazy('client:cardiologist_index')


class CardiologistView(ListView):
    template_name = 'clients/cardiologist/doctor_index.html'
    context_object_name = 'all_cardiologist'
    paginate_by = 10

    def get_queryset(self):
        return Cardiologist.objects.all()


class CardiologistDetailView(DetailView, DeleteView, CreateView):
    model = Cardiologist
    template_name = 'clients/cardiologist/doctor_detail.html'
    success_url = reverse_lazy('client:cardiologist_index')
    #fields= ['record_number','first_name', 'last_name', 'dob', 'device_man', 'device_name', 'implant_date', 'device_serial', 'bol_voltage','eri_voltage']
    form_class = CardiologistForm


class CardiologistDelete(DeleteView):
    model = Cardiologist
    success_url = reverse_lazy('client:cardiologist_index')


class CardiologistUpdate(UpdateView):
    model = Cardiologist
    template_name = 'clients/cardiologist/doctor_detail.html'
    form_class = CardiologistForm


class CardiologistSearchList(ListView):
    template_name = 'clients/cardiologist/doctor_search.html'
    model = Cardiologist
    context_object_name = 'results_list'

    def get_queryset(self):
        search = self.request.GET.get('search')
        queryset = Cardiologist.objects.filter(Q(last_name__icontains=search)|Q(first_name__icontains=search))
        return queryset



# General Paractitioner

class GeneralPractitionerCreate(CreateView):
    model = General_Practitioner
    template_name = 'clients/gp/doctor_new.html'
    form_class = General_PractitionerForm
    success_url = reverse_lazy('client:generalpractitioner_index')


class GeneralPractitionerView(ListView):
    template_name = 'clients/gp/doctor_index.html'
    context_object_name = 'all_GeneralPractitioner'
    paginate_by = 10

    def get_queryset(self):
        return General_Practitioner.objects.all()


class GeneralPractitionerDetailView(DetailView, DeleteView, CreateView):
    model = General_Practitioner
    template_name = 'clients/gp/doctor_detail.html'
    success_url = reverse_lazy('client:generalgractitioner_index')
    form_class = General_PractitionerForm

class GeneralPractitionerUpdate(UpdateView):
    model = General_Practitioner
    template_name = 'clients/gp/doctor_update_form.html'
    form_class = General_PractitionerForm
    #success_url = reverse_lazy('client:generalgractitioner_detail')
    template_name_suffix = '_update_form'


class GeneralPractitionerDelete(DeleteView):
    model = General_Practitioner
    success_url = reverse_lazy('client:generalpractitioner_index')
    success_message = 'Success: Client Deleted.'


class GeneralPractitionerSearchList(ListView):
    template_name = 'clients/gp/doctor_search.html'
    model = General_Practitioner
    context_object_name = 'results_list'

    def get_queryset(self):
        search = self.request.GET.get('search')
        queryset = General_Practitioner.objects.filter(Q(last_name__icontains=search)|Q(first_name__icontains=search))
        return queryset